-- CREATE TABLE product
CREATE TABLE IF NOT EXISTS `product` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`guid`	VARCHAR ( 50 ) NOT NULL UNIQUE,
	`sku`	VARCHAR ( 50 ) NOT NULL UNIQUE,
	`name`	VARCHAR ( 255 ) NOT NULL,
	`info_current_stock`	INTEGER,
	`sys_created_at`	DATETIME,
	`sys_updated_at`	DATETIME,
	`sys_soft_deleted`	INTEGER,
	`sys_deleted_at`	DATETIME
);

-- INSERT DATA into table product
-- Via Migrations folder

-- CREATE INDEX for table product
CREATE INDEX IF NOT EXISTS `idx_product_sku` ON `product` (
	`sku`
);

-- CREATE TABLE purchase
CREATE TABLE IF NOT EXISTS `purchase` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`guid`	VARCHAR ( 50 ) NOT NULL UNIQUE,
	`product_id`	INTEGER NOT NULL,
	`purchase_order_date`	DATETIME NOT NULL,
	`purchase_order_item_n`	INTEGER NOT NULL DEFAULT 0,
	`purchase_order_item_price`	NUMERIC NOT NULL,
	`purchase_order_status`	VARCHAR ( 30 ) NOT NULL,
	`delivered_status`	VARCHAR ( 30 ) NOT NULL,
	`delivered_item_n`	INTEGER NOT NULL,
	`delivered_receipt_no`	VARCHAR ( 50 ) NOT NULL,
	`delivered_note`	TEXT,
	`stock_in_status`	VARCHAR ( 20 ) NOT NULL,
	`sys_created_at`	DATETIME,
	`sys_updated_at`	DATETIME,
	`sys_soft_deleted`	INTEGER NOT NULL,
	`sys_deleted_at`	DATETIME
);

-- INSERT DATA into table purchase
-- Via Migrations folder

-- CREATE INDEX for table purchase
-- 

-- CREATE TABLE distribute
CREATE TABLE IF NOT EXISTS `distribute` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`guid`	VARCHAR ( 50 ) NOT NULL UNIQUE,
	`product_id`	INTEGER NOT NULL,
	`distribute_date`	DATETIME NOT NULL,
	`distribute_item_n`	INTEGER NOT NULL,
	`selling_price_item`	NUMERIC NOT NULL,
	`distribute_note`	TEXT,
	`distribute_status`	VARCHAR ( 20 ) NOT NULL,
	`related_order_number`	VARCHAR ( 50 ),
	`stock_out_status`	VARCHAR ( 20 ) NOT NULL,
	`sys_created_at`	DATETIME,
	`sys_updated_at`	DATETIME,
	`sys_soft_deleted`	INTEGER NOT NULL DEFAULT 0,
	`sys_deleted_at`	DATETIME
);

-- INSERT DATA into table distribute
-- Via Migrations folder

-- CREATE INDEX for table distribute
-- 
