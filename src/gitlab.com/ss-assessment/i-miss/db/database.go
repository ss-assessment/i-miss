package db

import (
	"os"
	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	config "../config"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
)

// Database type
type Database struct {
	Configuration *config.Configuration
}

// Init Database function
func (db *Database) Init(filename string) {
	orm.RegisterDriver("sqlite", orm.DRSqlite)

	db.Configuration = &config.Configuration{}
	err := db.Configuration.Load(filename)

	if err != nil {
		panic(err)
	} else {
		sqliteDBFile := db.Configuration.Database.DBFile

		if _, err := os.Stat(sqliteDBFile); os.IsNotExist(err) {
			//fmt.Println("File does not exist")
			panic(err)
		} else {

			err := orm.RegisterDataBase("default", "sqlite3", sqliteDBFile)

			if err != nil {
				panic(err)
			}

			orm.Debug = db.Configuration.Database.Debug
			//use default
			o := orm.NewOrm()
			o.Using("default")
		}
	}
}
