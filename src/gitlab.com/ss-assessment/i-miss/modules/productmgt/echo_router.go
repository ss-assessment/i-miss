package productmgt

import (
	"github.com/labstack/echo"
)

// ProductMgtRouter type Product Mgt Router
type ProductMgtRouter struct {
	APIPath string
}

// RegisterAllFeatures for handle http RegisterAllFeatures
func (R *ProductMgtRouter) RegisterAllFeatures(APISvr *echo.Echo) error {

	APISvr.POST(R.APIPath+"products", addProduct)
	APISvr.GET(R.APIPath+"products", getAllProduct)
	APISvr.GET(R.APIPath+"products/:keyword/:keyvalue", getProductByKeyWorld)
	APISvr.PUT(R.APIPath+"product/by-guid/:guid", updateProduct)
	APISvr.DELETE(R.APIPath+"product/by-guid/:guid", deleteProduct)

	APISvr.GET(R.APIPath+"export/product/csv", exportProductCsv)

	return nil
}
