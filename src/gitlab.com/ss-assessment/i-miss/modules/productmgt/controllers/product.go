package controllers

import (
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_product "../models"
)

// ProductController controller for Product
type ProductController struct{}

// GetProductByGUID function to Get Product By GUID
func (pC *ProductController) GetProductByGUID(uuid string) (interface{}, int, error) {
	var myErr error

	vReturn, err := model_product.GetProductByGUID(uuid)
	if err != nil {
		myErr = err
	}

	vCount := 1
	if vReturn.Id == 0 {
		vCount = 0
	}

	return vReturn, vCount, myErr
}

// GetProductBySKU function to Get Product By SKU
func (pC *ProductController) GetProductBySKU(sku string) (interface{}, int, error) {
	var myErr error

	vReturn, err := model_product.GetProductBySKU(sku)
	if err != nil {
		myErr = err
	}

	vCount := 1
	if vReturn.Id == 0 {
		vCount = 0
	}

	return vReturn, vCount, myErr
}

// GetAllProducts function to Get All Product using pagination and order
func (pC *ProductController) GetAllProducts(page int, order string, count bool, limit int) (interface{}, error) {
	var myErr error
	var vErr error
	var vReturn interface{}

	if count == true {
		_, resultCount, err := model_product.GetAllProducts(page, "notSorting", count, limit)
		vErr = err
		vReturn = resultCount
	} else {
		resultObj, _, err := model_product.GetAllProducts(page, "notSorting", count, limit)
		vErr = err
		vReturn = resultObj
	}

	if vErr != nil {
		myErr = vErr
	}

	return vReturn, myErr
}

// AddProduct function to Add Product
func (pC *ProductController) AddProduct(p *model_product.Product) (interface{}, error) {
	var myErr error

	p.SysCreatedAt = time.Now()
	p.SysUpdatedAt = time.Now()
	p.SysSoftDeleted = 0

	vReturn, err := model_product.AddProduct(p)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// UpdateProduct function to Update Product
func (pC *ProductController) UpdateProduct(p *model_product.Product) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	p.SysUpdatedAt = time.Now()
	vReturn, err := model_product.UpdateProduct(p)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// DeleteProduct function to Delete Product
func (pC *ProductController) DeleteProduct(p *model_product.Product) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	p.SysSoftDeleted = 1
	p.SysDeletedAt = time.Now()
	vReturn, err := model_product.DeleteProduct(p)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}
