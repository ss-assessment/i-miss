package productmgt

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	controller_product "./controllers"
	model_product "./models"
	"github.com/labstack/echo"
)

// productController type
var productController = controller_product.ProductController{}

// InputProduct type
type inputProduct struct {
	//Guid string `json:"guid"`
	Sku  string `json:"sku"`
	Name string `json:"name"`
}

// getProductByKeyWorld function to handle http.getProductByGUID
func getProductByKeyWorld(c echo.Context) (err error) {
	var vReturn interface{}
	var vCount int

	// get input
	keyword := strings.ToLower(c.Param("keyword"))
	keyvalue := strings.TrimSpace(c.Param("keyvalue"))

	// call related controller action
	switch keyword {
	case "by-guid":
		vResult, countF, errF := productController.GetProductByGUID(keyvalue)

		vReturn = vResult
		vCount = countF
		err = errF
	case "by-sku":
		vResult, countF, errF := productController.GetProductBySKU(keyvalue)

		vReturn = vResult
		vCount = countF
		err = errF
	default:
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vResponse)
	}

	status := true
	if err != nil {
		status = false
	}

	// send response
	httpStatus := http.StatusOK
	vResponse := map[string]interface{}{}
	if vCount == 0 {
		httpStatus = http.StatusNotFound
		vResponse = map[string]interface{}{
			"status": status,
			"response": map[string]interface{}{
				"data":  nil,
				"count": vCount,
			},
		}
	} else {
		vResponse = map[string]interface{}{
			"status": status,
			"response": map[string]interface{}{
				"data":  vReturn,
				"count": vCount,
			},
		}
	}

	return c.JSON(httpStatus, vResponse)
}

// getAllProduct function to handle http.getAllProduct
func getAllProduct(c echo.Context) (err error) {
	// get input
	page := c.QueryParam("page")
	pageI, err := strconv.Atoi(page)
	limit := c.QueryParam("limit")
	limitI, err := strconv.Atoi(limit)

	// call related controller action
	vReturn, err := productController.GetAllProducts(pageI, "notSorting", false, limitI)
	vCount, err2 := productController.GetAllProducts(pageI, "notSorting", true, limitI)

	status := true
	if err != nil {
		status = false
	}
	if err2 != nil {
		status = false
	}

	// send response
	vResponse := map[string]interface{}{
		"status": status,
		"response": map[string]interface{}{
			"data":  vReturn,
			"count": vCount,
		},
	}

	return c.JSON(http.StatusOK, vResponse)
}

// addProduct function to handle http.addProduct
func addProduct(c echo.Context) (err error) {

	// get input
	input := new(inputProduct)
	if errI := c.Bind(input); errI != nil {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// check input
	if strings.TrimSpace(input.Sku) == "" || strings.TrimSpace(input.Name) == "" {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter. Please assign 'Sku' and 'Name' data.",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// assign data
	p := new(model_product.Product)
	p.Name = input.Name
	p.Sku = input.Sku

	// call add product controller
	vReturn, err := productController.AddProduct(p)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":     input,
			"data_key": vReturn,
		},
	}

	c.Response().Header().Add("location", fmt.Sprintf("%v/by-guid/%v", c.Request().URL.String(), vReturn))
	c.Response().Header().Add("location", fmt.Sprintf("%v/by-sku/%v", c.Request().URL.String(), p.Sku))
	c.Response().Header().Add("x-http-method-location", "GET")

	return c.JSON(http.StatusOK, vResponse)
}

// updateProduct function to handle http.UpdateProduct
func updateProduct(c echo.Context) (err error) {

	// get input
	input := new(inputProduct)
	if errI := c.Bind(input); errI != nil {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// check input
	if strings.TrimSpace(input.Sku) == "" || strings.TrimSpace(input.Name) == "" {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter. Please assign 'Sku' and 'Name' data.",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// get guid
	guid := strings.ToLower(c.Param("guid"))

	// assign data
	p := new(model_product.Product)
	p.Guid = guid
	p.Sku = input.Sku
	p.Name = input.Name

	// call update data controller
	vReturn, err := productController.UpdateProduct(p)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":          input,
			"data_affected": vReturn,
		},
	}

	if fmt.Sprintf("%v", vReturn) != "0" {
		c.Response().Header().Add("location", fmt.Sprintf("/api/v1.0/products/by-guid/%v", guid))
		c.Response().Header().Add("x-http-method-location", "GET")
	}

	return c.JSON(http.StatusOK, vResponse)
}

// deleteProduct function to handle http.DeleteProduct
func deleteProduct(c echo.Context) (err error) {

	// get input
	guid := strings.ToLower(c.Param("guid"))

	// assign data
	p := new(model_product.Product)
	p.Guid = guid

	// call delete product controller action
	vReturn, err := productController.DeleteProduct(p)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":          guid,
			"data_affected": vReturn,
		},
	}

	if fmt.Sprintf("%v", vReturn) != "0" {
		c.Response().Header().Add("location", fmt.Sprintf("/api/v1.0/products/by-guid/%v", guid))
		c.Response().Header().Add("x-http-method-location", "GET")
	}

	return c.JSON(http.StatusOK, vResponse)
}

// exportProductCsv function to exportProductCsv
func exportProductCsv(c echo.Context) (err error) {
	res := c.Response()
	w := csv.NewWriter(res)

	header := res.Header()
	header.Set(echo.HeaderContentType, echo.MIMEOctetStream)

	fileName := fmt.Sprintf("Products-%v.csv", time.Now().Format("2006-01-02T15-04-05"))

	header.Set(echo.HeaderContentDisposition, "attachment; filename="+fileName)
	header.Set("Content-Transfer-Encoding", "binary")
	header.Set("Expires", "0")
	res.WriteHeader(http.StatusOK)

	vReturn, _, err := model_product.GetAllProductsExport(1, "notSorting", false, 50000000)

	csvheader := []string{"Id", "Guid", "SKU", "Name", "CreatedAt", "UpdateAt", "IsSoftDeleted", "DeletedAt"}
	w.Write(csvheader)
	for _, r := range vReturn {
		sr := []string{fmt.Sprintf("%v", r.Id), r.Guid, r.Sku, r.Name, r.SysCreatedAt.String(), r.SysCreatedAt.String(), fmt.Sprintf("%v", r.SysSoftDeleted), r.SysDeletedAt.String()}
		if err = w.Write(sr); err != nil {
			return
		}
		w.Flush()
	}

	return
}
