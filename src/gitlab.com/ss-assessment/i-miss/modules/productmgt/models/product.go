package models

import (
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	my_utils "../../../utils"
	"github.com/astaxie/beego/orm"
	g_uuid "github.com/google/uuid"
)

// ConsDisplayProductLimit const DisplayProductLimit
const ConsDisplayProductLimit = 20

// Product type
type Product struct {
	Id               int64     `json:"id" orm:"PK"`
	Guid             string    `json:"guid"`
	Sku              string    `json:"sku"`
	Name             string    `json:"name"`
	InfoCurrentStock int       `json:"info_current_stock"`
	SysCreatedAt     time.Time `json:"sys_created_at" orm:"auto_now_add;type(datetime)"`
	SysUpdatedAt     time.Time `json:"sys_updated_at" orm:"auto_now;type(datetime)"`
	SysSoftDeleted   int       `json:"sys_soft_deleted"`
	SysDeletedAt     time.Time `json:"sys_deleted_at" orm:"type(datetime)"`
}

func init() {
	orm.RegisterModel(new(Product))
}

// AddProduct function to add new Product
func AddProduct(p *Product) (string, error) {
	var myErr error
	o := orm.NewOrm()
	p.Id = time.Now().UnixNano()
	p.Guid = g_uuid.New().String()
	_, err := o.Insert(p)

	if err != nil {
		myErr = err
	}

	return p.Guid, myErr
}

// GetProductBySKU function to get product by SKU
func GetProductBySKU(sku string) (*Product, error) {
	p := Product{Sku: sku}
	o := orm.NewOrm()
	err := o.Read(&p, "sku")
	return &p, err
}

// GetProductByGUID function to get product by UUID
func GetProductByGUID(uuid string) (*Product, error) {
	p := Product{Guid: uuid}
	o := orm.NewOrm()
	err := o.Read(&p, "guid")
	return &p, err
}

// GetAllProducts function to get all product - using pagination and order
func GetAllProducts(page int, order string, count bool, limit int) ([]Product, interface{}, error) {
	var myErr error
	page--
	if page < 0 {
		page = 0
	}
	if limit < 0 {
		limit = ConsDisplayProductLimit
	}
	if limit > 500 {
		limit = 500
	}

	o := orm.NewOrm()
	var productCollections []Product
	qrySetCmd := o.QueryTable("product")
	qrySetCmd = qrySetCmd.Filter("sys_soft_deleted", "0")
	var tmpCnt int64

	if count == true {
		cnt, _ := qrySetCmd.Count()
		tmpCnt = cnt
		//return productCollections, cnt, myErr
	} else {
		qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd, order)
		qrySetCmd.Offset(page * limit).Limit(limit).All(&productCollections)
		//return productCollections, nil, nil
	}
	return productCollections, tmpCnt, myErr
}

// GetAllProductsExport function to get all product - using pagination and order
func GetAllProductsExport(page int, order string, count bool, limit int) ([]Product, interface{}, error) {
	var myErr error
	page--
	if page < 0 {
		page = 0
	}
	if limit < 0 {
		limit = ConsDisplayProductLimit
	}

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("product")

	var productCollections []Product
	var tmpCnt int64

	if count == true {
		cnt, _ := qrySetCmd.Count()
		tmpCnt = cnt
		//return productCollections, cnt, myErr
	} else {
		qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd, order)
		qrySetCmd.Offset(page * limit).Limit(limit).All(&productCollections)
		//return productCollections, nil, nil
	}
	return productCollections, tmpCnt, myErr
}

// UpdateProduct function to update selected Product
func UpdateProduct(p *Product) (int64, error) {
	var myErr error

	o := orm.NewOrm()
	affected, err := o.QueryTable("product").Filter("guid", p.Guid).Update(orm.Params{
		"name":           p.Name,
		"sku":            p.Sku,
		"sys_updated_at": p.SysUpdatedAt,
	})
	if err != nil {
		myErr = err
	}

	return affected, myErr
}

// DeleteProduct funtion to "Soft" Delete selected product
func DeleteProduct(p *Product) (int64, error) {
	var myErr error

	o := orm.NewOrm()
	affected, err := o.QueryTable("product").Filter("guid", p.Guid).Update(orm.Params{
		"sys_soft_deleted": p.SysSoftDeleted,
		"sys_deleted_at":   p.SysDeletedAt,
	})

	if err != nil {
		myErr = err
	}

	return affected, myErr
}
