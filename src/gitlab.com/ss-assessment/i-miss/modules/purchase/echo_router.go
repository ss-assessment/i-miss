package purchase

import (
	"github.com/labstack/echo"
)

// PurchaseMgtRouter type Purchase Router
type PurchaseMgtRouter struct {
	APIPath string
}

// RegisterAllFeatures for handle http RegisterAllFeatures
func (R *PurchaseMgtRouter) RegisterAllFeatures(APISvr *echo.Echo) error {

	APISvr.POST(R.APIPath+"purchases", addPurchase)
	APISvr.GET(R.APIPath+"purchases", getAllPurchase)
	APISvr.GET(R.APIPath+"purchases/:keyword/:keyvalue", getPurchaseByKeyWorld)
	APISvr.PUT(R.APIPath+"purchase/by-guid/:guid", updatePurchase)
	APISvr.DELETE(R.APIPath+"purchase/by-guid/:guid", deletePurchase)

	APISvr.GET(R.APIPath+"export/purchase/csv", exportPurcaseCsv)

	return nil
}
