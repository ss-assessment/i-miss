package purchase

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_product "../productmgt/models"
	controller_purchase "./controllers"
	model_purchase "./models"
	"github.com/labstack/echo"
)

// purchaseController type
var purchaseController = controller_purchase.PurchaseController{}

// InputPurchase type
type inputPurchase struct {
	//Guid string `json:"guid"`
	Sku                    string  `json:"sku"`
	PurchaseOrderDate      string  `json:"purchase_order_date"`
	PurchaseOrderItemN     int64   `json:"purchase_order_item_n"`
	PurchaseOrderItemPrice float64 `json:"purchase_order_item_price"`
	PurchaseOrderStatus    string  `json:"purchase_order_status"` // DRAFT, CANCEL, DONE
	DeliveredStatus        string  `json:"delivered_status"`      // WAITING, DELIVER, CANCEL
	DeliveredItemN         int64   `json:"delivered_item_n"`
	DeliveredReceiptNo     string  `json:"delivered_receipt_no"`
	DeliveredNote          string  `json:"delivered_note"`
	StockInStatus          string  `json:"stock_in_status"` //YES, NO
}

// getPurchaseByKeyWorld function to handle http.getPurchaseByKeyWorld
func getPurchaseByKeyWorld(c echo.Context) (err error) {
	var vReturn interface{}
	var vCount int

	// get input
	keyword := strings.ToLower(c.Param("keyword"))
	keyvalue := strings.TrimSpace(c.Param("keyvalue"))

	// call related controller action
	switch keyword {
	case "by-guid":
		vResult, countF, errF := purchaseController.GetPurchaseByGUID(keyvalue)

		vReturn = vResult
		vCount = countF
		err = errF
	case "by-sku":
		vResult, countF, errF := purchaseController.GetPurchaseBySKU(keyvalue)

		vReturn = vResult
		vCount = countF
		err = errF
	default:
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vResponse)
	}

	status := true
	if err != nil {
		status = false
	}

	// send response
	httpStatus := http.StatusOK
	vResponse := map[string]interface{}{}
	if vCount == 0 {
		httpStatus = http.StatusNotFound
		vResponse = map[string]interface{}{
			"status": status,
			"response": map[string]interface{}{
				"data":  nil,
				"count": vCount,
			},
		}
	} else {
		vResponse = map[string]interface{}{
			"status": status,
			"response": map[string]interface{}{
				"data":  vReturn,
				"count": vCount,
			},
		}
	}

	return c.JSON(httpStatus, vResponse)
}

// getAllPurchase function to handle http.getAllPurchase
func getAllPurchase(c echo.Context) (err error) {
	// get input
	page := c.QueryParam("page")
	pageI, err := strconv.Atoi(page)
	limit := c.QueryParam("limit")
	limitI, err := strconv.Atoi(limit)

	// call related controller action
	vReturn, err := purchaseController.GetAllPurchases(pageI, "notSorting", false, limitI)
	vCount, err2 := purchaseController.GetAllPurchases(pageI, "notSorting", true, limitI)

	status := true
	if err != nil {
		status = false
	}
	if err2 != nil {
		status = false
	}

	// send response
	vResponse := map[string]interface{}{
		"status": status,
		"response": map[string]interface{}{
			"data":  vReturn,
			"count": vCount,
		},
	}

	return c.JSON(http.StatusOK, vResponse)
}

// addPurchase function to handle http.addPurchase
func addPurchase(c echo.Context) (err error) {

	// get input
	input := new(inputPurchase)
	if errI := c.Bind(input); errI != nil {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// check input
	if strings.TrimSpace(input.Sku) == "" ||
		strings.TrimSpace(input.PurchaseOrderDate) == "" {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter. Please assign 'Sku' and 'PurchaseOrderDate' data.",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	vProduct, err := model_product.GetProductBySKU(input.Sku)

	// assign data
	p := new(model_purchase.Purchase)
	p.Product = vProduct

	layout := "2006-01-02T15:04:05.00000000Z"
	//layout := "2006-01-02T15:04:00"
	//str := "2014-11-12T11:45:26.371Z"
	if len(input.PurchaseOrderDate) <= 19 {
		input.PurchaseOrderDate = input.PurchaseOrderDate + ".10011001Z"
	}
	t, err := time.Parse(layout, input.PurchaseOrderDate)

	if err != nil {
		t = time.Now()
	}
	p.PurchaseOrderDate = t

	p.PurchaseOrderItemN = input.PurchaseOrderItemN
	p.PurchaseOrderItemPrice = input.PurchaseOrderItemPrice
	p.PurchaseOrderStatus = input.PurchaseOrderStatus // DRAFT, CANCEL, DONE
	p.DeliveredStatus = input.DeliveredStatus         // WAITING, DELIVER, CANCEL
	p.DeliveredItemN = input.DeliveredItemN
	p.DeliveredReceiptNo = input.DeliveredReceiptNo
	p.DeliveredNote = input.DeliveredNote
	p.StockInStatus = input.StockInStatus //YES, NO

	// call add purchase controller
	vReturn, err := purchaseController.AddPurchase(p)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":     input,
			"data_key": vReturn,
		},
	}

	c.Response().Header().Add("location", fmt.Sprintf("%v/by-guid/%v", c.Request().URL.String(), vReturn))
	c.Response().Header().Add("location", fmt.Sprintf("%v/by-sku/%v", c.Request().URL.String(), input.Sku))
	c.Response().Header().Add("x-http-method-location", "GET")

	return c.JSON(http.StatusOK, vResponse)
}

// updatePurchase function to handle http.updatePurchase
func updatePurchase(c echo.Context) (err error) {

	// get input
	input := new(inputPurchase)
	if errI := c.Bind(input); errI != nil {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// check input
	if strings.TrimSpace(input.PurchaseOrderDate) == "" {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter. Please assign 'PurchaseOrderDate' data.",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// get guid
	guid := strings.ToLower(c.Param("guid"))

	// assign data
	p := new(model_purchase.Purchase)
	p.Guid = guid

	layout := "2006-01-02T15:04:05.00000000Z"
	//layout := "2006-01-02T15:04:00"
	//str := "2014-11-12T11:45:26.371Z"
	if len(input.PurchaseOrderDate) <= 19 {
		input.PurchaseOrderDate = input.PurchaseOrderDate + ".10011001Z"
	}
	t, err := time.Parse(layout, input.PurchaseOrderDate)

	if err != nil {
		t = time.Now()
	}
	p.PurchaseOrderDate = t

	p.PurchaseOrderItemN = input.PurchaseOrderItemN
	p.PurchaseOrderItemPrice = input.PurchaseOrderItemPrice
	p.PurchaseOrderStatus = input.PurchaseOrderStatus // DRAFT, CANCEL, DONE
	p.DeliveredStatus = input.DeliveredStatus         // WAITING, DELIVER, CANCEL
	p.DeliveredItemN = input.DeliveredItemN
	p.DeliveredReceiptNo = input.DeliveredReceiptNo
	p.DeliveredNote = input.DeliveredNote
	p.StockInStatus = input.StockInStatus //YES, NO

	// call update data controller
	vReturn, err := purchaseController.UpdatePurchase(p)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":          input,
			"data_affected": vReturn,
		},
	}

	if fmt.Sprintf("%v", vReturn) != "0" {
		c.Response().Header().Add("location", fmt.Sprintf("/api/v1.0/purchases/by-guid/%v", guid))
		c.Response().Header().Add("x-http-method-location", "GET")
	}

	return c.JSON(http.StatusOK, vResponse)
}

// deletePurchase function to handle http.deletePurchase
func deletePurchase(c echo.Context) (err error) {

	// get input
	guid := strings.ToLower(c.Param("guid"))

	// assign data
	p := new(model_purchase.Purchase)
	p.Guid = guid

	// call delete purchase controller action
	vReturn, err := purchaseController.DeletePurchase(p)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":          guid,
			"data_affected": vReturn,
		},
	}

	if fmt.Sprintf("%v", vReturn) != "0" {
		c.Response().Header().Add("location", fmt.Sprintf("/api/v1.0/purchases/by-guid/%v", guid))
		c.Response().Header().Add("x-http-method-location", "GET")
	}

	return c.JSON(http.StatusOK, vResponse)
}

// exportPurcaseCsv function to exportPurcaseCsv
func exportPurcaseCsv(c echo.Context) (err error) {
	res := c.Response()
	w := csv.NewWriter(res)

	header := res.Header()
	header.Set(echo.HeaderContentType, echo.MIMEOctetStream)

	fileName := fmt.Sprintf("Purchase-%v.csv", time.Now().Format("2006-01-02T15-04-05"))

	header.Set(echo.HeaderContentDisposition, "attachment; filename="+fileName)
	header.Set("Content-Transfer-Encoding", "binary")
	header.Set("Expires", "0")
	res.WriteHeader(http.StatusOK)

	vReturn, _, err := model_purchase.GetAllPurchaseExport(1, "notSorting", false, 2000000000)

	vReturn2 := controller_purchase.CalculateTotalField(vReturn)

	csvheader := []string{"Id", "Guid", "ProductId",
		"SKU", "Name", "Waktu",
		"JumlahPemesanan", "JumlahDiterima", "HargaBeli", "Total",
		"NomorKwitansi", "Catatan",
		"StatusPemesanan", "StatusPenerimaan", "StockInStatus",
		"CreatedAt", "UpdateAt", "IsSoftDeleted", "DeletedAt"}

	w.Write(csvheader)
	for _, r := range vReturn2 {

		viewTotal := fmt.Sprintf("%.0f", r.ViewTotalPurcaseItemPrice)

		sr := []string{
			fmt.Sprintf("%v", r.Id), r.Guid, fmt.Sprintf("%v", r.Product.Id),
			r.Product.Sku, r.Product.Name,
			r.PurchaseOrderDate.String(), fmt.Sprintf("%v", r.PurchaseOrderItemN), fmt.Sprintf("%v", r.DeliveredItemN), fmt.Sprintf("%v", r.PurchaseOrderItemPrice), viewTotal,
			r.DeliveredReceiptNo, r.DeliveredNote,
			r.PurchaseOrderStatus, r.DeliveredStatus, r.StockInStatus,
			r.SysCreatedAt.String(), r.SysCreatedAt.String(), fmt.Sprintf("%v", r.SysSoftDeleted), r.SysDeletedAt.String()}

		if err = w.Write(sr); err != nil {
			return
		}
		w.Flush()
	}

	return
}
