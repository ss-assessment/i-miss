package models

import (
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	my_utils "../../../utils"
	model_product "../../productmgt/models"
	"github.com/astaxie/beego/orm"
	g_uuid "github.com/google/uuid"
)

// ConsDisplayPurchaseLimit const ConsDisplayPurchaseLimit
const ConsDisplayPurchaseLimit = 20

// Purchase type
type Purchase struct {
	Id                     int64                  `json:"id" orm:"PK"`
	Guid                   string                 `json:"guid"`
	Product                *model_product.Product `json:"product" orm:"rel(one)" valid:"Entity(Product)"`
	PurchaseOrderDate      time.Time              `json:"purchase_order_date"`
	PurchaseOrderItemN     int64                  `json:"purchase_order_item_n"`
	PurchaseOrderItemPrice float64                `json:"purchase_order_item_price"`
	PurchaseOrderStatus    string                 `json:"purchase_order_status"` // DRAFT, CANCEL, DONE
	DeliveredStatus        string                 `json:"delivered_status"`      // WAITING, DELIVER, CANCEL
	DeliveredItemN         int64                  `json:"delivered_item_n"`
	DeliveredReceiptNo     string                 `json:"delivered_receipt_no"`
	DeliveredNote          string                 `json:"delivered_note"`
	StockInStatus          string                 `json:"stock_in_status"` //YES, NO
	SysCreatedAt           time.Time              `json:"sys_created_at" orm:"auto_now_add;type(datetime)"`
	SysUpdatedAt           time.Time              `json:"sys_updated_at" orm:"auto_now;type(datetime)"`
	SysSoftDeleted         int                    `json:"sys_soft_deleted"`
	SysDeletedAt           time.Time              `json:"sys_deleted_at" orm:"type(datetime)"`
}

func init() {
	orm.RegisterModel(new(Purchase))
}

// AddPurchase function to add new Purchase
func AddPurchase(p *Purchase) (string, error) {
	var myErr error

	o := orm.NewOrm()
	p.Id = time.Now().UnixNano()
	p.Guid = g_uuid.New().String()

	_, err := o.Insert(p)

	if err != nil {
		myErr = err
	}

	return p.Guid, myErr
}

// GetPurchaseByProductID function to get purchase by UUID
func GetPurchaseByProductID(id int64) ([]Purchase, error) {
	var myErr error
	var purchaseCollections []Purchase

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("purchase")
	qrySetCmd = qrySetCmd.Filter("product_id", id)
	qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd.RelatedSel("Product"), "notSorting")
	_, err := qrySetCmd.All(&purchaseCollections)

	if err != nil {
		myErr = err
	}

	return purchaseCollections, myErr
}

// GetPurchaseByGUID function to get purchase by UUID
func GetPurchaseByGUID(uuid string) (*Purchase, error) {
	p := Purchase{Guid: uuid}
	o := orm.NewOrm()
	err := o.Read(&p, "guid")
	if p.Product != nil {
		o.Read(p.Product)
	}

	return &p, err
}

// GetAllPurchases function to get all purchase - using pagination and order
func GetAllPurchases(page int, order string, count bool, limit int) ([]Purchase, interface{}, error) {
	var myErr error
	page--
	if page < 0 {
		page = 0
	}
	if limit < 0 {
		limit = ConsDisplayPurchaseLimit
	}
	if limit > 500 {
		limit = 500
	}

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("purchase")
	qrySetCmd = qrySetCmd.Filter("sys_soft_deleted", "0")

	var purchaseCollections []Purchase
	var tmpCnt int64

	if count == true {
		cnt, _ := qrySetCmd.Count()
		tmpCnt = cnt
		//return purchaseCollections, cnt, myErr
	} else {
		qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd.RelatedSel("Product"), order)
		qrySetCmd.Offset(page * limit).Limit(limit).All(&purchaseCollections)
		//return productCollections, nil, nil
	}
	return purchaseCollections, tmpCnt, myErr
}

// GetAllPurchaseExport function to get all purchase - using pagination and order
func GetAllPurchaseExport(page int, order string, count bool, limit int) ([]Purchase, interface{}, error) {
	var myErr error
	page--
	if page < 0 {
		page = 0
	}
	if limit < 0 {
		limit = ConsDisplayPurchaseLimit
	}

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("purchase")

	var purchaseCollections []Purchase
	var tmpCnt int64

	if count == true {
		cnt, _ := qrySetCmd.Count()
		tmpCnt = cnt
		//return purchaseCollections, cnt, myErr
	} else {
		qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd.RelatedSel("Product"), order)
		qrySetCmd.Offset(page * limit).Limit(limit).All(&purchaseCollections)
		//return productCollections, nil, nil
	}
	return purchaseCollections, tmpCnt, myErr
}

// UpdatePurchase function to update selected purchase
func UpdatePurchase(p *Purchase) (int64, error) {
	var myErr error

	o := orm.NewOrm()
	affected, err := o.QueryTable("purchase").Filter("guid", p.Guid).Update(orm.Params{
		"purchase_order_date":       p.PurchaseOrderDate,
		"purchase_order_item_n":     p.PurchaseOrderItemN,
		"purchase_order_item_price": p.PurchaseOrderItemPrice,
		"purchase_order_status":     p.PurchaseOrderStatus, // DRAFT, CANCEL, DONE
		"delivered_status":          p.DeliveredStatus,     // WAITING, DELIVER, CANCEL
		"delivered_item_n":          p.DeliveredItemN,
		"delivered_receipt_no":      p.DeliveredReceiptNo,
		"delivered_note":            p.DeliveredNote,
		"stock_in_status":           p.StockInStatus, //YES, NO
		"sys_updated_at":            p.SysUpdatedAt,
	})
	if err != nil {
		myErr = err
	}

	return affected, myErr
}

// DeletePurchase funtion to "Soft" Delete selected purchase
func DeletePurchase(p *Purchase) (int64, error) {
	var myErr error

	o := orm.NewOrm()
	affected, err := o.QueryTable("purchase").Filter("guid", p.Guid).Update(orm.Params{
		"sys_soft_deleted": p.SysSoftDeleted,
		"sys_deleted_at":   p.SysDeletedAt,
	})

	if err != nil {
		myErr = err
	}

	return affected, myErr
}
