package controllers

import (
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_product "../../productmgt/models"
	model_purchase "../models"
)

// PurchaseController controller for Purchase
type PurchaseController struct{}

// GetPurchaseByGUID function to Get Purchase By GUID
func (pC *PurchaseController) GetPurchaseByGUID(uuid string) (interface{}, int, error) {
	var myErr error

	vReturn, err := model_purchase.GetPurchaseByGUID(uuid)
	if err != nil {
		myErr = err
	}

	vCount := 1
	if vReturn.Id == 0 {
		vCount = 0
	}

	var tmp []model_purchase.Purchase
	tmp = append(tmp, *vReturn)
	vReturn2 := CalculateTotalField(tmp)

	return vReturn2, vCount, myErr
}

// GetPurchaseBySKU function to Get Purchase By SKU
func (pC *PurchaseController) GetPurchaseBySKU(sku string) (interface{}, int, error) {
	var myErr error

	vProduct, err := model_product.GetProductBySKU(sku)

	vReturn, err := model_purchase.GetPurchaseByProductID(vProduct.Id)
	if err != nil {
		myErr = err
	}

	vCount := len(vReturn)

	vReturn2 := CalculateTotalField(vReturn)

	return vReturn2, vCount, myErr
}

// GetAllPurchases function to Get All Purchase using pagination and order
func (pC *PurchaseController) GetAllPurchases(page int, order string, count bool, limit int) (interface{}, error) {
	var myErr error
	var vErr error
	var vReturn interface{}

	if count == true {
		_, resultCount, err := model_purchase.GetAllPurchases(page, "notSorting", count, limit)
		vErr = err
		vReturn = resultCount
	} else {
		resultObj, _, err := model_purchase.GetAllPurchases(page, "notSorting", count, limit)
		vErr = err
		//vReturn = resultObj
		vReturn = CalculateTotalField(resultObj)
	}

	if vErr != nil {
		myErr = vErr
	}

	return vReturn, myErr
}

// AddPurchase function to Add Purchase
func (pC *PurchaseController) AddPurchase(p *model_purchase.Purchase) (interface{}, error) {
	var myErr error

	p.SysCreatedAt = time.Now()
	p.SysUpdatedAt = time.Now()
	p.SysSoftDeleted = 0

	vReturn, err := model_purchase.AddPurchase(p)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// UpdatePurchase function to Update Purchase
func (pC *PurchaseController) UpdatePurchase(p *model_purchase.Purchase) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	p.SysUpdatedAt = time.Now()
	vReturn, err := model_purchase.UpdatePurchase(p)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// DeletePurchase function to Delete Purchase
func (pC *PurchaseController) DeletePurchase(p *model_purchase.Purchase) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	p.SysSoftDeleted = 1
	p.SysDeletedAt = time.Now()
	vReturn, err := model_purchase.DeletePurchase(p)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// ViewPurcase for field calculation
type ViewPurchase struct {
	Id                        int64                  `json:"id"`
	Guid                      string                 `json:"guid"`
	Product                   *model_product.Product `json:"product"`
	PurchaseOrderDate         time.Time              `json:"purchase_order_date"`
	PurchaseOrderItemN        int64                  `json:"purchase_order_item_n"`
	PurchaseOrderItemPrice    float64                `json:"purchase_order_item_price"`
	ViewTotalPurcaseItemPrice float64                `json:"view_total_purchase_item_price"`
	PurchaseOrderStatus       string                 `json:"purchase_order_status"` // DRAFT, CANCEL, DONE
	DeliveredStatus           string                 `json:"delivered_status"`      // WAITING, DELIVER, CANCEL
	DeliveredItemN            int64                  `json:"delivered_item_n"`
	DeliveredReceiptNo        string                 `json:"delivered_receipt_no"`
	DeliveredNote             string                 `json:"delivered_note"`
	StockInStatus             string                 `json:"stock_in_status"` //YES, NO
	SysCreatedAt              time.Time              `json:"sys_created_at"`
	SysUpdatedAt              time.Time              `json:"sys_updated_at"`
	SysSoftDeleted            int                    `json:"sys_soft_deleted"`
	SysDeletedAt              time.Time              `json:"sys_deleted_at"`
}

// CalculateTotalField function to calculateTotalField
func CalculateTotalField(purchCollection []model_purchase.Purchase) []ViewPurchase {
	var vReturn []ViewPurchase

	for _, purch := range purchCollection {
		tmpPurc := ViewPurchase{}
		tmpPurc.Id = purch.Id
		tmpPurc.Guid = purch.Guid
		tmpPurc.Product = purch.Product

		tmpPurc.PurchaseOrderDate = purch.PurchaseOrderDate
		tmpPurc.PurchaseOrderItemN = purch.PurchaseOrderItemN
		tmpPurc.PurchaseOrderItemPrice = purch.PurchaseOrderItemPrice

		i := tmpPurc.PurchaseOrderItemN
		p := tmpPurc.PurchaseOrderItemPrice
		r := float64(i) * p
		tmpPurc.ViewTotalPurcaseItemPrice = float64(r)

		tmpPurc.PurchaseOrderStatus = purch.PurchaseOrderStatus
		tmpPurc.DeliveredStatus = purch.DeliveredStatus
		tmpPurc.DeliveredItemN = purch.DeliveredItemN
		tmpPurc.DeliveredReceiptNo = purch.DeliveredReceiptNo
		tmpPurc.DeliveredNote = purch.DeliveredNote
		tmpPurc.StockInStatus = purch.StockInStatus

		tmpPurc.SysCreatedAt = purch.SysCreatedAt
		tmpPurc.SysUpdatedAt = purch.SysUpdatedAt
		tmpPurc.SysSoftDeleted = purch.SysSoftDeleted
		tmpPurc.SysDeletedAt = purch.SysDeletedAt

		vReturn = append(vReturn, tmpPurc)
	}

	return vReturn
}
