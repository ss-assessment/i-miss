package distribution

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_product "../productmgt/models"
	controller_distribute "./controllers"
	model_distribute "./models"
	"github.com/labstack/echo"
)

// distributeController type
var distributeController = controller_distribute.DistributeController{}

// InputDistribute type
type inputDistribute struct {
	//Guid string `json:"guid"`
	Sku                string  `json:"sku"`
	DistributeDate     string  `json:"distribute_date"`
	DistributeItemN    int64   `json:"distribute_item_n"`
	SellingPriceItem   float64 `json:"selling_price_item"`
	DistributeNote     string  `json:"distibution_note"`
	RelatedOrderNumber string  `json:"related_order_number"`
	DistributeStatus   string  `json:"distribution_status"` // DRAFT, CANCEL, DONE, BROKEN
	StockOutStatus     string  `json:"stock_out_status"`    // YES, NO
}

// getDistributeByKeyWorld function to handle http.getDistributeByKeyWorld
func getDistributeByKeyWorld(c echo.Context) (err error) {
	var vReturn interface{}
	var vCount int

	// get input
	keyword := strings.ToLower(c.Param("keyword"))
	keyvalue := strings.TrimSpace(c.Param("keyvalue"))

	// call related controller action
	switch keyword {
	case "by-guid":
		vResult, countF, errF := distributeController.GetDistributeByGUID(keyvalue)

		vReturn = vResult
		vCount = countF
		err = errF
	case "by-sku":
		vResult, countF, errF := distributeController.GetDistributeBySKU(keyvalue)

		vReturn = vResult
		vCount = countF
		err = errF
	default:
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vResponse)
	}

	status := true
	if err != nil {
		status = false
	}

	// send response
	httpStatus := http.StatusOK
	vResponse := map[string]interface{}{}
	if vCount == 0 {
		httpStatus = http.StatusNotFound
		vResponse = map[string]interface{}{
			"status": status,
			"response": map[string]interface{}{
				"data":  nil,
				"count": vCount,
			},
		}
	} else {
		vResponse = map[string]interface{}{
			"status": status,
			"response": map[string]interface{}{
				"data":  vReturn,
				"count": vCount,
			},
		}
	}

	return c.JSON(httpStatus, vResponse)
}

// getAllDistribute function to handle http.getAllDistribute
func getAllDistribute(c echo.Context) (err error) {
	// get input
	page := c.QueryParam("page")
	pageI, err := strconv.Atoi(page)
	limit := c.QueryParam("limit")
	limitI, err := strconv.Atoi(limit)

	// call related controller action
	vReturn, err := distributeController.GetAllDistributes(pageI, "notSorting", false, limitI)
	vCount, err2 := distributeController.GetAllDistributes(pageI, "notSorting", true, limitI)

	status := true
	if err != nil {
		status = false
	}
	if err2 != nil {
		status = false
	}

	// send response
	vResponse := map[string]interface{}{
		"status": status,
		"response": map[string]interface{}{
			"data":  vReturn,
			"count": vCount,
		},
	}

	return c.JSON(http.StatusOK, vResponse)
}

// addDistribute function to handle http.addDistribute
func addDistribute(c echo.Context) (err error) {

	// get input
	input := new(inputDistribute)
	if errI := c.Bind(input); errI != nil {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// check input
	if strings.TrimSpace(input.Sku) == "" ||
		strings.TrimSpace(input.DistributeDate) == "" {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter. Please assign 'Sku' and 'DistributeDate' data.",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	vProduct, err := model_product.GetProductBySKU(input.Sku)

	// assign data
	d := new(model_distribute.Distribute)
	d.Product = vProduct

	layout := "2006-01-02T15:04:05.00000000Z"
	//layout := "2006-01-02T15:04:01"
	//str := "2014-11-12T11:45:26.371Z"
	if len(input.DistributeDate) <= 19 {
		input.DistributeDate = input.DistributeDate + ".10011001Z"
	}
	t, err := time.Parse(layout, input.DistributeDate)

	if err != nil {
		t = time.Now()
	}
	d.DistributeDate = t

	d.DistributeItemN = input.DistributeItemN
	d.SellingPriceItem = input.SellingPriceItem
	d.DistributeNote = input.DistributeNote
	d.DistributeStatus = input.DistributeStatus // DRAFT, CANCEL, DONE, BROKEN
	d.RelatedOrderNumber = input.RelatedOrderNumber
	d.StockOutStatus = input.StockOutStatus //YES, NO

	// call add Distribute controller
	vReturn, err := distributeController.AddDistribute(d)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":     input,
			"data_key": vReturn,
		},
	}

	c.Response().Header().Add("location", fmt.Sprintf("%v/by-guid/%v", c.Request().URL.String(), vReturn))
	c.Response().Header().Add("location", fmt.Sprintf("%v/by-sku/%v", c.Request().URL.String(), input.Sku))
	c.Response().Header().Add("x-http-method-location", "GET")

	return c.JSON(http.StatusOK, vResponse)
}

// updateDistribute function to handle http.updateDistribute
func updateDistribute(c echo.Context) (err error) {

	// get input
	input := new(inputDistribute)
	if errI := c.Bind(input); errI != nil {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// check input
	if strings.TrimSpace(input.DistributeDate) == "" {
		vReturn := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": "Invalid Request parameter. Please assign 'DistributeDate' data.",
			},
		}
		return c.JSON(http.StatusBadRequest, vReturn)
	}

	// get guid
	guid := strings.ToLower(c.Param("guid"))

	// assign data
	d := new(model_distribute.Distribute)
	d.Guid = guid

	layout := "2006-01-02T15:04:05.00000000Z"
	//layout := "2006-01-02T15:04:00"
	//str := "2014-11-12T11:45:26.371Z"
	if len(input.DistributeDate) <= 19 {
		input.DistributeDate = input.DistributeDate + ".10011001Z"
	}
	t, err := time.Parse(layout, input.DistributeDate)

	if err != nil {
		t = time.Now()
	}
	d.DistributeDate = t

	d.DistributeItemN = input.DistributeItemN
	d.SellingPriceItem = input.SellingPriceItem
	d.DistributeNote = input.DistributeNote
	d.DistributeStatus = input.DistributeStatus // DRAFT, CANCEL, DONE, BROKEN
	d.RelatedOrderNumber = input.RelatedOrderNumber
	d.StockOutStatus = input.StockOutStatus //YES, NO

	// call update data controller
	vReturn, err := distributeController.UpdateDistribute(d)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":          input,
			"data_affected": vReturn,
		},
	}

	if fmt.Sprintf("%v", vReturn) != "0" {
		c.Response().Header().Add("location", fmt.Sprintf("/api/v1.0/distributes/by-guid/%v", guid))
		c.Response().Header().Add("x-http-method-location", "GET")
	}

	return c.JSON(http.StatusOK, vResponse)
}

// deleteDistribute function to handle http.deleteDistribute
func deleteDistribute(c echo.Context) (err error) {

	// get input
	guid := strings.ToLower(c.Param("guid"))

	// assign data
	d := new(model_distribute.Distribute)
	d.Guid = guid

	// call delete Distribute controller action
	vReturn, err := distributeController.DeleteDistribute(d)

	// send error
	if err != nil {
		vResponse := map[string]interface{}{
			"status": false,
			"response": map[string]interface{}{
				"error": fmt.Sprintf("%v", err),
			},
		}

		return c.JSON(http.StatusConflict, vResponse)
	}

	// send response
	vResponse := map[string]interface{}{
		"status": true,
		"response": map[string]interface{}{
			"data":          guid,
			"data_affected": vReturn,
		},
	}

	if fmt.Sprintf("%v", vReturn) != "0" {
		c.Response().Header().Add("location", fmt.Sprintf("/api/v1.0/distributes/by-guid/%v", guid))
		c.Response().Header().Add("x-http-method-location", "GET")
	}

	return c.JSON(http.StatusOK, vResponse)
}

// exportDistributeCsv function to exportDistributeCsv
func exportDistributeCsv(c echo.Context) (err error) {
	res := c.Response()
	w := csv.NewWriter(res)

	header := res.Header()
	header.Set(echo.HeaderContentType, echo.MIMEOctetStream)

	fileName := fmt.Sprintf("Distribute-%v.csv", time.Now().Format("2006-01-02T15-04-05"))

	header.Set(echo.HeaderContentDisposition, "attachment; filename="+fileName)
	header.Set("Content-Transfer-Encoding", "binary")
	header.Set("Expires", "0")
	res.WriteHeader(http.StatusOK)

	vReturn, _, err := model_distribute.GetAllDistributeExport(1, "notSorting", false, 2000000000)

	vReturn2 := controller_distribute.CalculateTotalField(vReturn)

	csvheader := []string{"Id", "Guid", "ProductId",
		"SKU", "Name", "Waktu",
		"JumlahKeluar", "HargaJual", "Total",
		"NomorOrder", "Catatan",
		"StatusDistribusi", "StockOutStatus",
		"CreatedAt", "UpdateAt", "IsSoftDeleted", "DeletedAt"}

	w.Write(csvheader)
	for _, r := range vReturn2 {

		viewTotal := fmt.Sprintf("%.0f", r.ViewTotalItemPrice)

		sr := []string{
			fmt.Sprintf("%v", r.Id), r.Guid, fmt.Sprintf("%v", r.Product.Id),
			r.Product.Sku, r.Product.Name, r.DistributeDate.String(),
			fmt.Sprintf("%v", r.DistributeItemN), fmt.Sprintf("%v", r.SellingPriceItem), viewTotal,
			r.RelatedOrderNumber, r.DistributeNote,
			r.DistributeStatus, r.StockOutStatus,
			r.SysCreatedAt.String(), r.SysCreatedAt.String(), fmt.Sprintf("%v", r.SysSoftDeleted), r.SysDeletedAt.String()}

		if err = w.Write(sr); err != nil {
			return
		}
		w.Flush()
	}

	return
}
