package controllers

import (
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_product "../../productmgt/models"
	model_distribute "../models"
)

// DistributeController controller for Distribute
type DistributeController struct{}

// GetDistributeByGUID function to Get Distribute By GUID
func (pC *DistributeController) GetDistributeByGUID(uuid string) (interface{}, int, error) {
	var myErr error

	vReturn, err := model_distribute.GetDistributeByGUID(uuid)
	if err != nil {
		myErr = err
	}

	vCount := 1
	if vReturn.Id == 0 {
		vCount = 0
	}

	var tmp []model_distribute.Distribute
	tmp = append(tmp, *vReturn)
	vReturn2 := CalculateTotalField(tmp)

	return vReturn2, vCount, myErr
}

// GetDistributeBySKU function to Get Distribute By SKU
func (pC *DistributeController) GetDistributeBySKU(sku string) (interface{}, int, error) {
	var myErr error

	vProduct, err := model_product.GetProductBySKU(sku)

	vReturn, err := model_distribute.GetDistributeByProductID(vProduct.Id)
	if err != nil {
		myErr = err
	}

	vCount := len(vReturn)

	vReturn2 := CalculateTotalField(vReturn)

	return vReturn2, vCount, myErr
}

// GetAllDistributes function to Get All Distribute using pagination and order
func (pC *DistributeController) GetAllDistributes(page int, order string, count bool, limit int) (interface{}, error) {
	var myErr error
	var vErr error
	var vReturn interface{}

	if count == true {
		_, resultCount, err := model_distribute.GetAllDistributes(page, "notSorting", count, limit)
		vErr = err
		vReturn = resultCount
	} else {
		resultObj, _, err := model_distribute.GetAllDistributes(page, "notSorting", count, limit)
		vErr = err
		//vReturn = resultObj
		vReturn = CalculateTotalField(resultObj)
	}

	if vErr != nil {
		myErr = vErr
	}

	return vReturn, myErr
}

// AddDistribute function to Add Distribute
func (pC *DistributeController) AddDistribute(d *model_distribute.Distribute) (interface{}, error) {
	var myErr error

	d.SysCreatedAt = time.Now()
	d.SysUpdatedAt = time.Now()
	d.SysSoftDeleted = 0

	vReturn, err := model_distribute.AddDistribute(d)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// UpdateDistribute function to Update Distribute
func (pC *DistributeController) UpdateDistribute(d *model_distribute.Distribute) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	d.SysUpdatedAt = time.Now()
	vReturn, err := model_distribute.UpdateDistribute(d)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// DeleteDistribute function to Delete Distribute
func (pC *DistributeController) DeleteDistribute(d *model_distribute.Distribute) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	d.SysSoftDeleted = 1
	d.SysDeletedAt = time.Now()
	vReturn, err := model_distribute.DeleteDistribute(d)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}

// ViewDistribute type for Field Calculation
type ViewDistribute struct {
	Id                 int64                  `json:"id"`
	Guid               string                 `json:"guid"`
	Product            *model_product.Product `json:"product"`
	DistributeDate     time.Time              `json:"distribute_date"`
	DistributeItemN    int64                  `json:"distribute_item_n"`
	SellingPriceItem   float64                `json:"selling_price_item"`
	ViewTotalItemPrice float64                `json:"view_total_item_price"`
	DistributeNote     string                 `json:"distribute_note"`
	RelatedOrderNumber string                 `json:"related_order_number"`
	DistributeStatus   string                 `json:"distribute_status"` // DRAFT, CANCEL, DONE, BROKEN
	StockOutStatus     string                 `json:"stock_out_status"`  // YES, NO
	SysCreatedAt       time.Time              `json:"sys_created_at"`
	SysUpdatedAt       time.Time              `json:"sys_updated_at"`
	SysSoftDeleted     int                    `json:"sys_soft_deleted"`
	SysDeletedAt       time.Time              `json:"sys_deleted_at"`
}

// CalculateTotalField function to calculateTotalField
func CalculateTotalField(disCollection []model_distribute.Distribute) []ViewDistribute {
	var vReturn []ViewDistribute

	for _, dis := range disCollection {
		tmpDis := ViewDistribute{}
		tmpDis.Id = dis.Id
		tmpDis.Guid = dis.Guid
		tmpDis.Product = dis.Product
		tmpDis.DistributeDate = dis.DistributeDate
		tmpDis.DistributeItemN = dis.DistributeItemN
		tmpDis.SellingPriceItem = dis.SellingPriceItem

		i := tmpDis.DistributeItemN
		p := tmpDis.SellingPriceItem
		r := float64(i) * p
		tmpDis.ViewTotalItemPrice = float64(r)

		tmpDis.DistributeNote = dis.DistributeNote
		tmpDis.RelatedOrderNumber = dis.RelatedOrderNumber
		tmpDis.DistributeStatus = dis.DistributeStatus
		tmpDis.StockOutStatus = dis.StockOutStatus

		tmpDis.SysCreatedAt = dis.SysCreatedAt
		tmpDis.SysUpdatedAt = dis.SysUpdatedAt
		tmpDis.SysSoftDeleted = dis.SysSoftDeleted
		tmpDis.SysDeletedAt = dis.SysDeletedAt

		vReturn = append(vReturn, tmpDis)
	}
	return vReturn
}
