package distribution

import (
	"github.com/labstack/echo"
)

// DistributeMgtRouter type Distribute Router
type DistributeMgtRouter struct {
	APIPath string
}

// RegisterAllFeatures for handle http RegisterAllFeatures
func (R *DistributeMgtRouter) RegisterAllFeatures(APISvr *echo.Echo) error {

	APISvr.POST(R.APIPath+"distributes", addDistribute)
	APISvr.GET(R.APIPath+"distributes", getAllDistribute)
	APISvr.GET(R.APIPath+"distributes/:keyword/:keyvalue", getDistributeByKeyWorld)
	APISvr.PUT(R.APIPath+"distribute/by-guid/:guid", updateDistribute)
	APISvr.DELETE(R.APIPath+"distribute/by-guid/:guid", deleteDistribute)

	APISvr.GET(R.APIPath+"export/distribute/csv", exportDistributeCsv)

	return nil
}
