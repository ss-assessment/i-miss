package models

import (
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	my_utils "../../../utils"
	model_product "../../productmgt/models"
	"github.com/astaxie/beego/orm"
	g_uuid "github.com/google/uuid"
)

// ConsDisplayDistributeLimit const ConsDisplayDistributeLimit
const ConsDisplayDistributeLimit = 20

// Distribute type
type Distribute struct {
	Id                 int64                  `json:"id" orm:"PK"`
	Guid               string                 `json:"guid"`
	Product            *model_product.Product `json:"product" orm:"rel(one)" valid:"Entity(Product)"`
	DistributeDate     time.Time              `json:"distribute_date"`
	DistributeItemN    int64                  `json:"distribute_item_n"`
	SellingPriceItem   float64                `json:"selling_price_item"`
	DistributeNote     string                 `json:"distribute_note"`
	RelatedOrderNumber string                 `json:"related_order_number"`
	DistributeStatus   string                 `json:"distribute_status"` // DRAFT, CANCEL, DONE, BROKEN
	StockOutStatus     string                 `json:"stock_out_status"`  // YES, NO
	SysCreatedAt       time.Time              `json:"sys_created_at" orm:"auto_now_add;type(datetime)"`
	SysUpdatedAt       time.Time              `json:"sys_updated_at" orm:"auto_now;type(datetime)"`
	SysSoftDeleted     int                    `json:"sys_soft_deleted"`
	SysDeletedAt       time.Time              `json:"sys_deleted_at" orm:"type(datetime)"`
}

func init() {
	orm.RegisterModel(new(Distribute))
}

// AddDistribute function to add new Distribute
func AddDistribute(d *Distribute) (string, error) {
	var myErr error

	o := orm.NewOrm()
	d.Id = time.Now().UnixNano()
	d.Guid = g_uuid.New().String()

	_, err := o.Insert(d)

	if err != nil {
		myErr = err
	}

	return d.Guid, myErr
}

// GetDistributeByProductID function to get Distribute by UUID
func GetDistributeByProductID(id int64) ([]Distribute, error) {
	var myErr error
	var DistributeCollections []Distribute

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("Distribute")
	qrySetCmd = qrySetCmd.Filter("product_id", id)
	qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd.RelatedSel("Product"), "notSorting")
	_, err := qrySetCmd.All(&DistributeCollections)

	if err != nil {
		myErr = err
	}

	return DistributeCollections, myErr
}

// GetDistributeByGUID function to get Distribute by UUID
func GetDistributeByGUID(uuid string) (*Distribute, error) {
	d := Distribute{Guid: uuid}
	o := orm.NewOrm()
	err := o.Read(&d, "guid")
	if d.Product != nil {
		o.Read(d.Product)
	}

	return &d, err
}

// GetAllDistributes function to get all Distribute - using pagination and order
func GetAllDistributes(page int, order string, count bool, limit int) ([]Distribute, interface{}, error) {
	var myErr error
	page--
	if page < 0 {
		page = 0
	}
	if limit < 0 {
		limit = ConsDisplayDistributeLimit
	}
	if limit > 500 {
		limit = 500
	}

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("Distribute")
	qrySetCmd = qrySetCmd.Filter("sys_soft_deleted", "0")

	var DistributeCollections []Distribute
	var tmpCnt int64

	if count == true {
		cnt, _ := qrySetCmd.Count()
		tmpCnt = cnt
		//return DistributeCollections, cnt, myErr
	} else {
		qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd.RelatedSel("Product"), order)
		qrySetCmd.Offset(page * limit).Limit(limit).All(&DistributeCollections)
		//return productCollections, nil, nil
	}
	return DistributeCollections, tmpCnt, myErr
}

// GetAllDistributeExport function to get all Distribute - using pagination and order
func GetAllDistributeExport(page int, order string, count bool, limit int) ([]Distribute, interface{}, error) {
	var myErr error
	page--
	if page < 0 {
		page = 0
	}
	if limit < 0 {
		limit = ConsDisplayDistributeLimit
	}

	o := orm.NewOrm()
	qrySetCmd := o.QueryTable("Distribute")

	var DistributeCollections []Distribute
	var tmpCnt int64

	if count == true {
		cnt, _ := qrySetCmd.Count()
		tmpCnt = cnt
		//return DistributeCollections, cnt, myErr
	} else {
		qrySetCmd = my_utils.ParseQuerySetterOrder(qrySetCmd.RelatedSel("Product"), order)
		qrySetCmd.Offset(page * limit).Limit(limit).All(&DistributeCollections)
		//return productCollections, nil, nil
	}
	return DistributeCollections, tmpCnt, myErr
}

// UpdateDistribute function to update selected Distribute
func UpdateDistribute(d *Distribute) (int64, error) {
	var myErr error

	o := orm.NewOrm()
	affected, err := o.QueryTable("Distribute").Filter("guid", d.Guid).Update(orm.Params{
		"distribute_date":      d.DistributeDate,
		"distribute_item_n":    d.DistributeItemN,
		"selling_price_item":   d.SellingPriceItem,
		"distribute_note":      d.DistributeNote,
		"distribute_status":    d.DistributeStatus, // DRAFT, CANCEL, DONE, BROKEN
		"related_order_number": d.RelatedOrderNumber,
		"stock_out_status":     d.StockOutStatus, // YES, NO
		"sys_updated_at":       d.SysUpdatedAt,
	})
	if err != nil {
		myErr = err
	}

	return affected, myErr
}

// DeleteDistribute funtion to "Soft" Delete selected Distribute
func DeleteDistribute(d *Distribute) (int64, error) {
	var myErr error

	o := orm.NewOrm()
	affected, err := o.QueryTable("Distribute").Filter("guid", d.Guid).Update(orm.Params{
		"sys_soft_deleted": d.SysSoftDeleted,
		"sys_deleted_at":   d.SysDeletedAt,
	})

	if err != nil {
		myErr = err
	}

	return affected, myErr
}
