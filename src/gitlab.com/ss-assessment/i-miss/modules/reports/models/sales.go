package models

import (
	"github.com/astaxie/beego/orm"
)

// ConsDisplayReportSalesLimit const ConsDisplayReportSalesLimit
const ConsDisplayReportSalesLimit = 20

// ReportSales Type
type ReportSales struct {
	NomorPesanan   string  `json:"nomor_pesanan"`
	Waktu          string  `json:"waktu"`
	Sku            string  `json:"sku"`
	Name           string  `json:"name"`
	Jumlah         int64   `json:"jumlah"`
	HargaJual      float64 `json:"harga_jual"`
	TotalHargaJual float64 `json:"total_harga_jual"`
	HargaBeli      float64 `json:"harga_beli"`
	Laba           float64 `json:"laba"`
}

func init() {
	//orm.RegisterModel(new(ReportProductValue))
}

// GetReportSales function GetReportSales
func GetReportSales(dateFrom string, dateTo string) ([]ReportSales, interface{}, error) {
	var myErr error
	var reportCollection []ReportSales

	sql := "SELECT dis.related_order_number nomor_pesanan, dis.distribute_date waktu, " +
		"    p.sku, p.name,  " +
		"	dis.distribute_item_n jumlah,  " +
		"	dis.selling_price_item harga_jual, " +
		"	(dis.distribute_item_n * dis.selling_price_item) total_harga_jual, " +
		"	vw_harga_beli.harga_beli_rerata harga_beli, " +
		"	(dis.distribute_item_n * dis.selling_price_item) - (dis.distribute_item_n * vw_harga_beli.harga_beli_rerata) laba " +
		" FROM product p, distribute dis, " +
		"	    (SELECT pur1.product_id, " +
		"		sum(pur1.purchase_order_item_price) /sum(pur1.delivered_item_n) harga_beli_rerata " +
		"		FROM product p1, purchase pur1 " +
		"		WHERE p1.id = pur1.product_id  " +
		"		GROUP BY p1.id) vw_harga_beli " +
		" WHERE p.id = dis.product_id " +
		" AND dis.product_id = vw_harga_beli.product_id " +
		" AND dis.distribute_date >= ? AND distribute_date <= ?"

	o := orm.NewOrm()

	num, err := o.Raw(sql, dateFrom, dateTo).QueryRows(&reportCollection)

	if err != nil {
		myErr = err
	}

	return reportCollection, num, myErr
}
