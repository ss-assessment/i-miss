package models

import (
	"github.com/astaxie/beego/orm"
)

// ConsDisplayReportProductValueLimit const ConsDisplayReportProductValueLimit
const ConsDisplayReportProductValueLimit = 20

// ReportProductValue Type
type ReportProductValue struct {
	Sku             string  `json:"sku"`
	Name            string  `json:"name"`
	Jumlah          int64   `json:"jumlah"`
	HargaBeliRerata float64 `json:"harga_beli_rerata"`
	TotalHarga      float64 `json:"total_harga"`
}

func init() {
	//orm.RegisterModel(new(ReportProductValue))
}

// GetReportProductValues function GetReportProductValues
func GetReportProductValues(datePrint string) ([]ReportProductValue, interface{}, error) {
	var myErr error
	var reportCollection []ReportProductValue

	sql := "SELECT p.sku, p.name, " +
		"	sum(pur.delivered_item_n) jumlah,  " +
		"	sum(pur.purchase_order_item_price) /sum(pur.delivered_item_n) harga_beli_rerata, " +
		"	sum(pur.purchase_order_item_price) total_harga " +
		" FROM product p, purchase pur " +
		" WHERE p.id = pur.product_id " +
		" AND pur.purchase_order_date <= ? " +
		" GROUP BY p.sku, p.name"

	o := orm.NewOrm()

	num, err := o.Raw(sql, datePrint).QueryRows(&reportCollection)

	if err != nil {
		myErr = err
	}

	return reportCollection, num, myErr
}
