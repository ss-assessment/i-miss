package reports

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"time"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	controller_report "./controllers"
	model_report "./models"
	"github.com/labstack/echo"
)

// reportController type
var reportController = controller_report.ReportProductValueController{}
var reportSalesController = controller_report.ReportSalesController{}

// getReportProductValues function to handle http.GetReportProductValues
func getReportProductValues(c echo.Context) (err error) {
	// get input
	datePrint := c.Param("date_print")
	if datePrint == "" {
		datePrint = time.Now().String()
		datePrint = datePrint[0:10]
	}

	// call related controller action
	vReturn, err := reportController.GetReportProductValues(datePrint)

	status := true
	if err != nil {
		status = false
	}
	//vCount := len(vReturn)
	// send response
	vResponse := map[string]interface{}{
		"status": status,
		"response": map[string]interface{}{
			"data": vReturn,
		},
	}

	return c.JSON(http.StatusOK, vResponse)
}

// exportReportProductValuesCsv function to exportReportProductValuesCsv
func exportReportProductValuesCsv(c echo.Context) (err error) {

	// get input
	datePrint := c.Param("date_print")
	if datePrint == "" {
		datePrint = time.Now().String()
		datePrint = datePrint[0:10]
	}

	res := c.Response()
	w := csv.NewWriter(res)

	header := res.Header()
	header.Set(echo.HeaderContentType, echo.MIMEOctetStream)

	fileName := fmt.Sprintf("NilaiBarang-%v.csv", time.Now().Format("2006-01-02T15-04-05"))

	header.Set(echo.HeaderContentDisposition, "attachment; filename="+fileName)
	header.Set("Content-Transfer-Encoding", "binary")
	header.Set("Expires", "0")
	res.WriteHeader(http.StatusOK)

	vReturn, _, err := model_report.GetReportProductValues(datePrint)

	csvheader := []string{
		"SKU", "Name", "Jumlah",
		"RerataHargaBeli", "Total"}

	w.Write(csvheader)
	for _, r := range vReturn {

		sr := []string{
			r.Sku, r.Name, fmt.Sprintf("%v", r.Jumlah),
			fmt.Sprintf("%.0f", r.HargaBeliRerata), fmt.Sprintf("%.0f", r.TotalHarga)}

		if err = w.Write(sr); err != nil {
			return
		}
		w.Flush()
	}

	return
}

// Sales
// getReportSales function to handle http.getReportSales
func getReportSales(c echo.Context) (err error) {
	// get input
	dateFrom := c.Param("date_from")
	if dateFrom == "" {
		dateFrom = time.Now().String()
		dateFrom = dateFrom[0:10]
		dateFrom = "2000-01-01"
	}

	dateTo := c.Param("date_to")
	if dateTo == "" {
		dateTo = time.Now().String()
		dateTo = dateTo[0:10]
	}

	// call related controller action
	vReturn, err := reportSalesController.GetReportSales(dateFrom, dateTo)

	status := true
	if err != nil {
		status = false
	}
	//vCount := len(vReturn)
	// send response
	vResponse := map[string]interface{}{
		"status": status,
		"response": map[string]interface{}{
			"data": vReturn,
		},
	}

	return c.JSON(http.StatusOK, vResponse)
}

// exportReportSalesCsv function to exportReportSalesCsv
func exportReportSalesCsv(c echo.Context) (err error) {

	// get input
	dateFrom := c.Param("date_from")
	if dateFrom == "" {
		dateFrom = time.Now().String()
		dateFrom = dateFrom[0:10]
		dateFrom = "2000-01-01"
	}

	dateTo := c.Param("date_to")
	if dateTo == "" {
		dateTo = time.Now().String()
		dateTo = dateTo[0:10]
	}

	res := c.Response()
	w := csv.NewWriter(res)

	header := res.Header()
	header.Set(echo.HeaderContentType, echo.MIMEOctetStream)

	fileName := fmt.Sprintf("Penjualan-%v.csv", time.Now().Format("2006-01-02T15-04-05"))

	header.Set(echo.HeaderContentDisposition, "attachment; filename="+fileName)
	header.Set("Content-Transfer-Encoding", "binary")
	header.Set("Expires", "0")
	res.WriteHeader(http.StatusOK)

	vReturn, _, err := model_report.GetReportSales(dateFrom, dateTo)

	csvheader := []string{
		"IDPesanan", "Waktu",
		"SKU", "Name", "Jumlah",
		"HargaJual", "TotalHargaJual",
		"HargaBeli", "Laba"}

	w.Write(csvheader)
	for _, r := range vReturn {

		sr := []string{
			r.NomorPesanan, r.Waktu[0:20],
			r.Sku, r.Name, fmt.Sprintf("%v", r.Jumlah),
			fmt.Sprintf("%.0f", r.HargaJual), fmt.Sprintf("%.0f", r.TotalHargaJual),
			fmt.Sprintf("%.0f", r.HargaBeli), fmt.Sprintf("%.0f", r.Laba)}

		if err = w.Write(sr); err != nil {
			return
		}
		w.Flush()
	}

	return
}
