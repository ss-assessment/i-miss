package controllers

import (
	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_prodvalue "../models"
)

// ReportProductValueController controller for ReportProductValueController
type ReportProductValueController struct{}

// GetReportProductValues function to GetReportProductValues using pagination and order
func (pC *ReportProductValueController) GetReportProductValues(datePrint string) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	vReturn, _, err := model_prodvalue.GetReportProductValues(datePrint)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}
