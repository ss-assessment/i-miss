package controllers

import (
	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	model_report "../models"
)

// ReportSalesController controller for ReportSalesController
type ReportSalesController struct{}

// GetReportSales function to GetReportSales using pagination and order
func (pC *ReportSalesController) GetReportSales(dateFrom string, dateTo string) (interface{}, error) {
	var myErr error
	var vReturn interface{}

	vReturn, _, err := model_report.GetReportSales(dateFrom, dateTo)

	if err != nil {
		myErr = err
	}

	return vReturn, myErr
}
