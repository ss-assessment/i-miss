package reports

import (
	"github.com/labstack/echo"
)

// ReportMgtRouter type Purchase Router
type ReportMgtRouter struct {
	APIPath string
}

// RegisterAllFeatures for handle http RegisterAllFeatures
func (R *ReportMgtRouter) RegisterAllFeatures(APISvr *echo.Echo) error {

	APISvr.GET(R.APIPath+"report/purchase/:date_print", getReportProductValues)
	APISvr.GET(R.APIPath+"report/distribute/:date_from/:date_to", getReportSales)

	APISvr.GET(R.APIPath+"export/report/purchase/csv/:date_print", exportReportProductValuesCsv)
	APISvr.GET(R.APIPath+"export/report/distribute/csv/:date_from/:date_to", exportReportSalesCsv)

	return nil
}
