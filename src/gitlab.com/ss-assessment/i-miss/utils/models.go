package utils

import (
	"strings"

	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.
	my_orm "github.com/astaxie/beego/orm"
)

// ParseQueryBuilderOrder function to Parser Query Builder Order Command
func ParseQueryBuilderOrder(qb my_orm.QueryBuilder, order string, entityAlias string) {
	if order != "notSorting" {
		dir := order[0]
		field := order[1:len(order)]
		if dir == '+' {
			qb.OrderBy(entityAlias + "." + field).Desc()
		} else {
			qb.OrderBy(entityAlias + "." + field).Asc()
		}
	}
}

// ParseQuerySetterOrder function to Parse Query Setter Order Command
func ParseQuerySetterOrder(qs my_orm.QuerySeter, order string) my_orm.QuerySeter {
	if order != "notSorting" {
		order = SnakeString(order)
		dir := order[0]
		field := order[1:len(order)]
		if dir == '+' {
			return qs.OrderBy(field)
		} else {
			return qs.OrderBy(order)
		}
	}
	return qs
}

// SnakeString function to make "snake string"
func SnakeString(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	return strings.ToLower(string(data[:len(data)]))
}
