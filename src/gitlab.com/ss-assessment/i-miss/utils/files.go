package utils

import (
	"os"
)

// GetAppConfigFile function to Get Application Config File
func GetAppConfigFile() string {
	filename := "dev_mode/config.dev.json"
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		//fmt.Println("File does not exist")
		panic(err)
	} else {
		//fmt.Println("File exists")
		disFilename := "../config/config.json"
		if _, err2 := os.Stat(disFilename); os.IsNotExist(err2) {
			// filename = filename
		} else {
			filename = disFilename
		}
	}

	return filename
}
