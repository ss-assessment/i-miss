package config

import (
	"encoding/json"
	"io/ioutil"
)

// ConfigurationListener type
type ConfigurationListener struct {
	Port string `json:"port"`
}

// ConfigurationSQLiteDatabase type
type ConfigurationSQLiteDatabase struct {
	Driver string `json:"driver"`
	DBFile string `json:"sqlite_db_file"`
	Debug  bool   `json:"debug"`
}

// Configuration type
type Configuration struct {
	Listener ConfigurationListener       `json:"listener"`
	Database ConfigurationSQLiteDatabase `json:"database"`
	//	PublicRoute []string
}

// Load function
func (configuration *Configuration) Load(filename string) (err error) {
	data, err := ioutil.ReadFile(filename)

	if err != nil {
		panic(filename + " was not found")
	}

	err = configuration.Parse(data)

	return err
}

// Parse function
func (configuration *Configuration) Parse(data []byte) (err error) {
	err = json.Unmarshal(data, &configuration)

	return
}
