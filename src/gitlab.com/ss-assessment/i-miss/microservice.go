package main

import (
	// We use "short path" package location for simplicity and portability. (for reqruitment testing purpose)
	// Please feel free to change with your requirement. e.g: using "Full Path" package location.

	app_db "./db"
	router_module_distribution "./modules/distribute"
	router_module_product "./modules/productmgt"
	router_module_purchase "./modules/purchase"
	router_module_report "./modules/reports"
	app_utils "./utils"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
)

// MiroServiceSvr REST Microservice Server
var MiroServiceSvr = echo.New()

func init() {
	appConfigFile := app_utils.GetAppConfigFile()
	DB := app_db.Database{}
	DB.Init(appConfigFile)
}

func main() {
	// Settting
	MiroServiceSvr.Logger.SetLevel(log.DEBUG)

	// Midleware
	MiroServiceSvr.Use(middleware.Logger())
	MiroServiceSvr.Use(middleware.Recover())

	// static www-public
	MiroServiceSvr.Static("/docs", "./dev_mode/www-public")

	// Path
	apiPath := "api/v1.0/"

	// Register all Product Modules
	modProduct := router_module_product.ProductMgtRouter{APIPath: apiPath}
	modProduct.RegisterAllFeatures(MiroServiceSvr)

	// Register all Purchase Modules
	modPurchase := router_module_purchase.PurchaseMgtRouter{APIPath: apiPath}
	modPurchase.RegisterAllFeatures(MiroServiceSvr)

	// Register all Distribution Modules
	modDistribution := router_module_distribution.DistributeMgtRouter{APIPath: apiPath}
	modDistribution.RegisterAllFeatures(MiroServiceSvr)

	// Regiter all Report Module
	modReport := router_module_report.ReportMgtRouter{APIPath: apiPath}
	modReport.RegisterAllFeatures(MiroServiceSvr)

	// start the server, and log if it fails
	MiroServiceSvr.Logger.Fatal(MiroServiceSvr.Start(":8787"))
}
