<p align="center">
  <a href="http://salestock.io/">
    <img src="https://www.salestockindonesia.com/assets/images/logo-ss-34f2d4dd.png" alt="SaleStock Tech" width=300>
  </a>

  <h3 align="center">Ijah - Mini Inventory System Services
 <strong>Distribution Document</strong> 
  <br>
  [ iMiss version: 1.0.0 ]</h3>

  <p align="center">
    Author:
    <br>
    <a href="https://gitlab.com/hi.muhammad.hari"><strong>Muhammad Hari Suharto »</strong></a>
    <br>
    <br>
    <a href="https://gitlab.com/ss-assessment/i-miss/issues">Report issues (need permission)</a>
  </p>
</p>

<br>

#### Purpose
This document will guide you on how to do:

- Build ;

- Run iMiss Application ;

<br>

#### Development Environment
Please note that for development purpose, I'm using a particular environment as follows:

- OS: macOS High Sierra, version 10.13.4 (17E202) ;

- GO: go version go1.10.3 darwin/amd64 ;

- IDE: Visual Studio Code, version 1.25.1 (1.25.1) ;

<br>

#### Distribrution Structure
```code
.
├── README.md [<--- We Are Here]
├── bin
│   ├── build
│   ├── functional_test
│   ├── pack-dist-src
│   │   ├── config
│   │   │   └── config.json
│   │   ├── database
│   │   │   └── i-miss.db
│   │   └── dev_mode
│   │       └── config.dev.json
│   ├── run_iMiss_Linux64
│   ├── run_iMiss_Mac64
│   ├── run_iMiss_Win64
│   ├── run_iMiss_default
│   └── test_data
│       ├── input-distribute
│       ├── input-product
│       └── input-purchase
├── distribution
│   ├── default
│   │   ├── bin
│   │   │   ├── dev_mode
│   │   │   │   └── config.dev.json
│   │   │   └── iMiss
│   │   ├── config
│   │   │   └── config.json
│   │   └── database
│   │       └── i-miss.db
│   ├── linux-64
│   │   ├── bin
│   │   │   ├── dev_mode
│   │   │   │   └── config.dev.json
│   │   │   └── iMiss
│   │   ├── config
│   │   │   └── config.json
│   │   └── database
│   │       └── i-miss.db
│   ├── mac-64
│   │   ├── bin
│   │   │   ├── dev_mode
│   │   │   │   └── config.dev.json
│   │   │   └── iMiss
│   │   ├── config
│   │   │   └── config.json
│   │   └── database
│   │       └── i-miss.db
│   └── win-64
│       ├── bin
│       │   ├── dev_mode
│       │   │   └── config.dev.json
│       │   └── iMiss.exe
│       ├── config
│       │   └── config.json
│       └── database
│           └── i-miss.db
├── src
│   └── gitlab.com
│       └── ss-assessment
│           └── i-miss
│               ├── config
│               │   ├── app.go
│               │   └── config.go
│               ├── db
│               │   ├── database.go
│               │   └── migrations
│               │       ├── 00-schema.sql
│               │       ├── 00100-init_data_product.sql
│               │       ├── 00200-init_data_purchase.sql
│               │       └── 00300-init_data_distribute.sql
│               ├── dev_mode
│               │   ├── config.dev.json
│               │   ├── i-miss.db
│               │   └── www-public
│               │       ├── api-explorer
│               │       │   └── index.html
│               │       └── statics
│               ├── glide.yaml
│               ├── microservice.go
│               ├── modules
│               │   ├── distribute
│               │   │   ├── controllers
│               │   │   │   └── distribute.go
│               │   │   ├── echo_handler.go
│               │   │   ├── echo_router.go
│               │   │   └── models
│               │   │       └── distribute.go
│               │   ├── productmgt
│               │   │   ├── controllers
│               │   │   │   └── product.go
│               │   │   ├── echo_handler.go
│               │   │   ├── echo_router.go
│               │   │   └── models
│               │   │       └── product.go
│               │   ├── purchase
│               │   │   ├── controllers
│               │   │   │   └── purchase.go
│               │   │   ├── echo_handler.go
│               │   │   ├── echo_router.go
│               │   │   └── models
│               │   │       └── purchase.go
│               │   └── reports
│               │       ├── controllers
│               │       │   ├── product_value.go
│               │       │   └── sales.go
│               │       ├── echo_handler.go
│               │       ├── echo_router.go
│               │       └── models
│               │           ├── product_value.go
│               │           └── sales.go
│               └── utils
│                   ├── files.go
│                   └── models.go
└── www-public
    └── statics
        └── index.html
```

<br>

#### Git History
We can found <strong>Git History (.git)</strong> on directory: <strong>./.git</strong>

Online Gitlab Repository:

- https://gitlab.com/ss-assessment/i-miss.git (Public Access);


<br>

### How to use
We need to uncompress a zip distribution file first. 
```code
unzip iMiss_100_hari.zip
```

<br>

### How to build
Command:
```code
cd i-miss/bin
chmod +x build
./build
```
Result:
```code
Build iMiss 1.0.0 Solution...
...
```

<br>

### Runing Compiled iMiss (binary)
Binary Files:

- run_iMiss_default (Default OS)

- run_iMiss_Mac64 (MacOs 64 Bit)

- run_iMiss_Linux64 (Linux OS 64 Bit)

- run_iMiss_Win64 (Windows OS 64 Bit)

Command:
```code
chmod +x run_iMiss_default (for the first time)
./run_iMiss_default
```
Result:
```code
http server started on [::]:8787
```


<br>

#### REST-API Resource

#### 1. Catatan Jumlah Barang (Product)


- ListAllProduct [GET]: http://localhost:8787/api/v1.0/products

```code
curl http://localhost:8787/api/v1.0/products?page=1&limit=100
```

<br>

- GetProductByGUID [GET]: http://localhost:8787/api/v1.0/products/by-guid/:guid

```code
curl http://localhost:8787/api/v1.0/products/by-guid/39c7b762-a9db-4b62-b34f-325d39c8abe0
```

<br>

- GetProductBySKU [GET]: http://localhost:8787/api/v1.0/products/by-sku/:sku

```code
curl http://localhost:8787/api/v1.0/products/by-sku/SSI-D00791015-LL-BWH
```

<br>

- AddProduct [POST]: http://localhost:8787/api/v1.0/products

```code
curl -X POST \
  http://localhost:8787/api/v1.0/products \
  -H 'Content-Type: application/json' \
  -d '{"name":"MHS-D00791015-LL-BWH", "sku": "My Product Name "}'
```

<br>

- UpdateProduct [PUT]: http://localhost:8787/api/v1.0/product/by-guid/:guid

```code
curl -X PUT \
  http://localhost:8787/api/v1.0/product/by-guid/d8c99baa-618c-432a-9151-87595711e2af \
  -H 'Content-Type: application/json' \
  -d '{"name":"My Product Name New", "sku": "MHS-D00791015-LL-BWH-N"}'
```

<br>

- DeleteProduct [DELETE] (Soft Delete): http://localhost:8787/api/v1.0/product/by-guid/:guid

```code
curl -X DELETE \
  http://localhost:8787/api/v1.0/product/by-guid/cfb972c6-e223-49b6-8697-883316595da3 \
  -H 'Content-Type: application/json'
```

<br>

- ExportProduct to CSV [GET] (Please Open via Browser): http://localhost:8787/api/v1.0/export/product/csv

```code
curl http://localhost:8787/api/v1.0/export/product/csv
```

<br>

#### 2. Catatan Barang Masuk

- ListAllPurchase [GET]: http://localhost:8787/api/v1.0/purchases

```code
curl http://localhost:8787/api/v1.0/purchases?page=1&limit=100
```

<br>

- GetPurchaseByGUID [GET]: http://localhost:8787/api/v1.0/purchases/by-guid/:guid

```code
curl http://localhost:8787/api/v1.0/purchases/by-guid/7b0302fd-f5a0-4657-a2da-6103719ebfb0
```

<br>

- GetPurchaseBySKU [GET]: http://localhost:8787/api/v1.0/purchases/by-sku/:sku

```code
curl http://localhost:8787/api/v1.0/purchases/by-sku/SSI-D01401071-LL-RED
```

<br>

- AddPurchase [POST]: http://localhost:8787/api/v1.0/purchases

```code
curl -X POST \
  http://localhost:8787/api/v1.0/purchases \
  -H 'Content-Type: application/json' \
  -d '{
  "sku": "SSI-D01401071-LL-RED",
  "purchase_order_date": "2018-01-02T11:20:00",
  "purchase_order_item_n": 62,
  "purchase_order_item_price": 74000,
  "delivered_item_n": 47,
  "delivered_receipt_no": "20180102-69539",
  "delivered_note": "2018/01/06 terima 47; Masih Menunggu",
  "purchase_order_status": "DONE",
  "delivered_status": "WAITING",
  "stock_in_status": "YES"
}'
```

<br>

- UpdatePurcase [PUT]: http://localhost:8787/api/v1.0/purchase/by-guid/:guid

```code
curl -X PUT \
  http://localhost:8787/api/v1.0/purchase/by-guid/69a4169a-d383-4833-8d4b-8e35a064a009 \
  -H 'Content-Type: application/json' \
  -d '{
	"purchase_order_date": "2018-12-21T13:13",
	"purchase_order_item_n": 100,
	"purchase_order_item_price": 10000,
	"delivered_item_n": 100,
	"delivered_receipt_no": "KW-20181225-123456",
	"delivered_note": "2018/12/26 terima 100",
	"purchase_order_status": "DONE",
	"delivered_status": "DELIVER",
	"stock_in_status": "YES"
}'
```

<br>

- DeletePurchase [DELETE] (Soft Delete): http://localhost:8787/api/v1.0/purchase/by-guid/:guid

```code
curl -X DELETE \
  http://localhost:8787/api/v1.0/purchase/by-guid/4997c215-7321-481c-a73d-1c22fec9b760 \
  -H 'Content-Type: application/json'
```

<br>

- ExportPurchase to CSV [GET] (Please Open via Browser): http://localhost:8787/api/v1.0/export/purchase/csv

```code
curl http://localhost:8787/api/v1.0/export/purchase/csv
```

<br>

#### 3. Catatan Barang Keluar

- ListAllDistribute [GET]: http://localhost:8787/api/v1.0/distributes

```code
curl http://localhost:8787/api/v1.0/distributes?page=1&limit=100
```

<br>

- GetDistributeByGUID [GET]: http://localhost:8787/api/v1.0/distributes/by-guid/:guid

```code
curl http://localhost:8787/api/v1.0/distributes/by-guid/cb19b1f4-b582-422d-8ce8-1124ffaea423
```

<br>

- GetDistributeBySKU [GET]: http://localhost:8787/api/v1.0/distributes/by-sku/:sku

```code
curl http://localhost:8787/api/v1.0/distributes/by-sku/SSI-D01401050-MM-RED
```

<br>

- AddDistribute [POST]: http://localhost:8787/api/v1.0/distributes

```code
curl -X POST \
  http://localhost:8787/api/v1.0/distributes \
  -H 'Content-Type: application/json' \
  -d '{
  "sku": "SSI-D01326299-LL-NAV",
  "distribute_date": "2017-09-08T15:55:58",
  "distribute_item_n": 1,
  "selling_price_item": 0,
  "distibution_note": "Barang Rusak",
  "related_order_number": "",
  "distribution_status": "BROKEN",
  "stock_out_status": "YES"
}'
```

<br>

- UpdateDistribute [PUT]: http://localhost:8787/api/v1.0/distribute/by-guid/:guid

```code
curl -X PUT \
  http://localhost:8787/api/v1.0/distribute/by-guid/d0113730-1e17-4f70-a6a7-8919157be8ba \
  -H 'Content-Type: application/json' \
  -d '{
	"distribute_date": "2017-05-18T07.29.09",
	"distribute_item_n": 1,
	"selling_price_item": 10000,
	"distibution_note": "Pesanan ID-20170518-173253",
	"related_order_number": "ID-20170518-173253",
	"distribution_status": "DONE",
	"stock_out_status": "YES"
}''
```

<br>

- DeleteDistribute [DELETE] (Soft Delete): http://localhost:8787/api/v1.0/distribute/by-guid/:guid

```code
curl -X DELETE \
  http://localhost:8787/api/v1.0/distribute/by-guid/d0113730-1e17-4f70-a6a7-8919157be8ba \
  -H 'Content-Type: application/json'
```

<br>

- ExportDistribute to CSV [GET] (Please Open via Browser): http://localhost:8787/api/v1.0/export/distribute/csv

```code
curl http://localhost:8787/api/v1.0/export/distribute/csv
```

<br>

#### 4. Laporan

- LaporanNilaiBarang [GET]: http://localhost:8787/api/v1.0/report/purchase/:date_print

```code
curl http://localhost:8787/api/v1.0/report/purchase/2018-12-30
```

<br>

- LaporanNilaiBarang CSV [GET] (Please Open via Brower): http://localhost:8787/api/v1.0/export/report/purchase/csv/:date_print

```code
curl http://localhost:8787/api/v1.0/export/report/purchase/csv/2018-12-30
```

<br>

- LaporanPenjualan [GET]: http://localhost:8787/api/v1.0/report/distribute/:date_from/:date_to

```code
curl http://localhost:8787/api/v1.0/report/distribute/2017-01-01/2018-12-30
```

<br>

- LaporanPenjualan CSV [GET] (Please Open via Brower): http://localhost:8787/api/v1.0/export/report/distribute/csv/:date_from/:date_to

```code
curl http://localhost:8787/api/v1.0/export/report/distribute/csv/2017-01-01/2018-12-30
```

<br>

#### Question
If you have any question, please feel free to contact <a href="https://gitlab.com/hi.muhammad.hari"><strong>Muhammad Hari Suharto »</strong></a>.